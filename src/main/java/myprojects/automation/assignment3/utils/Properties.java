package myprojects.automation.assignment3.utils;

import com.sun.deploy.util.WinRegistry;

/**
 * Help class to get passed parameters from environment for further usage in the automation project
 */
public class Properties {

    /**
     *
     * @return The name of the browser that need to be used for execution.
     */
    public static String getBrowser() {
        String defaultBrowserWinReg = WinRegistry.getString(WinRegistry.HKEY_CLASSES_ROOT,"htmlfile\\shell\\open\\command","");//get path to default  browser for WinReg
        String defaultBrowserName;

        if (defaultBrowserWinReg.contains("iexplore"))
            defaultBrowserName="IE";
        else if (defaultBrowserWinReg.contains("chrome"))
            defaultBrowserName="CHROME";
        else if (defaultBrowserWinReg.contains("firefox"))
            defaultBrowserName="FIREFOX";
        else defaultBrowserName=defaultBrowserWinReg;

        System.out.println("Your default browser is: "+defaultBrowserName);
        return defaultBrowserName;
    }

}

