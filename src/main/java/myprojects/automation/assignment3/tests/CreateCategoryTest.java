package myprojects.automation.assignment3.tests;

import myprojects.automation.assignment3.BaseScript;
import myprojects.automation.assignment3.GeneralActions;
import org.openqa.selenium.WebDriver;

public class CreateCategoryTest extends BaseScript {
    private static String login = "webinar.test@gmail.com";
    private static String password = "Xcg7299bnSmMuRLp9ITw";
    private static String categoryName = "Test";
    private static String categoryDescription = "AP test";


    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = BaseScript.getConfiguredDriver();
        GeneralActions actions = new GeneralActions(driver);
        actions.login(login, password); //login to the AdminPanel
        actions.createCategory(categoryName, categoryDescription); //create new category
        actions.checkCreatedCategory(categoryName, categoryDescription);//check that new category has been created
        actions.filterCategories(categoryName);
        driver.quit();
    }
}
