package myprojects.automation.assignment3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private static String BASE_URL_ADMINPANEL="http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    private By catalogueLink = By.cssSelector("#subtab-AdminCatalog");
    private By categoriesLink = By.cssSelector("#subtab-AdminCategories");
    private By newCategoryLink = By.cssSelector("a[id = 'page-header-desc-category-new_category']");
    private By successMessage = By.cssSelector("div[class='alert alert-success']");




    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.navigate().to(BASE_URL_ADMINPANEL);
        WebElement loginInput = driver.findElement(By.cssSelector("input[id='email']"));
        WebElement passwordInput = driver.findElement(By.cssSelector("#passwd"));
        WebElement loginButton = driver.findElement(By.cssSelector("[name='submitLogin']"));
        loginInput.sendKeys(login);
        passwordInput.sendKeys(password);
        loginButton.click();
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName, String categoryDescription) {

        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogueLink));
        WebElement catalogueLink = driver.findElement(this.catalogueLink);

        Actions actions = new Actions(driver);

        actions.moveToElement(catalogueLink).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoriesLink));
        WebElement categoriesLink = driver.findElement(this.categoriesLink);
        actions.moveToElement(categoriesLink).click().build().perform(); //click "Каталог->Категории"

        wait.until(ExpectedConditions.visibilityOfElementLocated(newCategoryLink));
        WebElement addCategory = driver.findElement(newCategoryLink); //"Добавить категорию" button
        addCategory.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#name_1")));
        WebElement categoryNameInput = driver.findElement(By.cssSelector("#name_1")); ///"Имя" input
        categoryNameInput.sendKeys(categoryName);

        WebElement categoryDescriptionField = driver.findElement(By.cssSelector("#description_1_ifr")); //"Описание" input
        categoryDescriptionField.sendKeys(" ");
        categoryDescriptionField.sendKeys(categoryDescription);

        WebElement submitNewCategoryFrom = driver.findElement(By.cssSelector("#category_form_submit_btn")); //"Сохранить" button
        submitNewCategoryFrom.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(successMessage));
        WebElement successMessage = driver.findElement(this.successMessage);
        System.out.println("Message is: '"+successMessage.getText()+"'");


    }

    public void checkCreatedCategory(String categoryName, String categoryDescription) {
        List<WebElement> tableRows = driver.findElements(By.cssSelector("#table-category td[class='pointer']"));
        int doubledRowsQty = tableRows.size(); //class - "pointer" is used for columns Имя and Описание - so list size is twice as categories qty
        String lastCategoryName = tableRows.get(doubledRowsQty-2).getText();
        String lastCategoryDescription = tableRows.get(doubledRowsQty-1).getText();
        if  (lastCategoryDescription.contains(categoryDescription) & (lastCategoryName.contains(categoryName)))
        System.out.println("Category '"+categoryName+"' with description '"+categoryDescription+"' was successfully created");
        else
        System.out.println("Test failed");
    }

    public void filterCategories(String categoryName) {
       WebElement filterByCategoryName = driver.findElement(By.cssSelector("input[name='categoryFilter_name']"));
       filterByCategoryName.sendKeys(categoryName);
       filterByCategoryName.submit();
       List<WebElement> tableRows = driver.findElements(By.cssSelector("#table-category td[class='pointer']"));
       int doubledRowsQty = tableRows.size(); //class - "pointer" is used for columns Имя and Описание - so list size is twice as categories qty
       if (doubledRowsQty==0)
         System.out.println("Category with name="+categoryName+" doesn't exist");
       else
         System.out.println("There are "+doubledRowsQty/2+" category(ies) with name="+categoryName);
    }
    }
